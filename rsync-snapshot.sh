#!/bin/bash
# ----------------------------------------------------------------------
# created by francois scheurer on 20070323
# derivate from mikes handy rotating-filesystem-snapshot utility
# see http://www.mikerubel.org/computers/rsync_snapshots
# ----------------------------------------------------------------------
#rsync note:
#    1) rsync -avz /src/foo  /dest      => ok, creates /dest/foo, like cp -a /src/foo /dest
#    2) rsync -avz /src/foo/ /dest/foo  => ok, creates /dest/foo, like cp -a /src/foo/. /dest/foo (or like cp -a /src/foo /dest)
#    3) rsync -avz /src/foo/ /dest/foo/ => ok, same as 2)
#    4) rsync -avz /src/foo/ /dest      => dangerous!!! overwrite dest content, like cp -a /src/foo/. /dest
#      solution: remove trailing / at /src/foo/ => 1)
#      minor problem: rsync -avz /src/foo /dest/foo => creates /dest/foo/foo, like mkdir /dest/foo && cp -a /src/foo /dest/foo
#    main options:
#      -H --hard-links
#      -a equals -rlptgoD (no -H,-A,-X)
#        -r --recursive
#        -l --links
#        -p --perms
#        -t --times
#        -g --group
#        -o --owner
#        -D --devices --specials
#      -x --one-file-system
#      -S --sparse
#      --numeric-ids
#    useful options:
#      -n --dry-run
#      -z --compress
#      -y --fuzzy
#      --bwlimit=X limit disk IO to X kB/s
#      -c --checksum
#      -I --ignore-times
#      --size-only
#    other options:
#      -v --verbose
#      -P equals --progress --partial
#      -h --human-readable
#      --stats
#      -e'ssh -o ServerAliveInterval=60'
#      --delete
#      --delete-delay
#      --delete-excluded
#      --ignore-existing
#      -i --itemize-changes
#      --stop-at
#      --time-limit
#      --rsh=\"ssh -p ${HOST_PORT} -i /root/.ssh/rsync_rsa -l root\" 
#      --rsync-path=\"/usr/bin/rsync\""
#    quickcheck options:
#      the default behavior is to skip files with same size & mtime on destination
#      mtime = last data write access
#      atime = last data read access (can be ignored with noatime mount option or with chattr +A)
#      ctime = last inode change (write access, change of permission or ownership)
#      note that a checksum is always done after a file synchronization/transfer
#      --modify-window=X ignore mtime differences less or equal to X sec
#      --size-only skip files with same size on destination (ignore mtime)
#      -c --checksum skip files with same MD5 checksum on destination (ignore size & mtime, all files are read once, then the list of files to be resynchronized is read a second time, there is a lot of disk IO but network trafic is minimal if many files are identical; log includes only different files)
#      -I --ignore-times never skip files (all files are resynchronized, all files are read once, there is more network trafic than with --checksum but less disk IO and hence is faster than --checksum if net is fast or if most files are different; log includes all files)
#      --link-dest does the quickcheck on another reference-directory and makes hardlinks if quickcheck succeeds
#        (however, if mtime is different and --perms is used, the reference file is copied in a new inode)
#    see also this link for a rsync tutorial: http://www.thegeekstuff.com/2010/09/rsync-command-examples/
#todo:
#                 'du' slow on many snapshot.X..done
#  autokill after n minutes.
#                 if disk full, its better to replace the snapshot.001 than to cancel and have a very old backup (even if it may fail to create the snapshot and ends with 0 backups)..done
#                 rsync-snapshot for oracle redo logs..old
#                 'find'-list with md5 signatures -> .gz file stored aside rsync.log.gz inside the snapshot.X folder; this file will be move to parent dir /backup/snapshot/localhost/ before deletion of a snapshot; this file will also be used to extract an incremental backup with tape-arch.sh..done (md5sum calculation with rsync-list.sh for acm14=18m58 and only 5m27 with a reference file. speedup is ~250-300%)
#  realtime freedisk display with echo $(($(stat -f -c "%f" /backup/snapshot/) * 4096 / 1024))
#  use authorized_keys with restriction of bash (command=) and set sshd_config with PermitRootLogin=forced-commands-only, see http://troy.jdmz.net/rsync/index.html http://www.snailbook.com/faq/restricted-scp.auto.html
#  note: rsync lists all files in snapshot.X disregarding inclusion patterns, this is slow.

fatal() {
	echo "FATAL: ${1}" >&2
	exit 2
}

error() {
	echo "ERROR: ${1}" >&2
	exit 1
}

warn() {
	echo "WARNING: ${1}"
}

info() {
	echo "INFO: ${1}"
}


do_timestamp() {
	${DATE} "+%Y/%m/%d %H:%M:%S"
}


###############################################################################
# Verify that we have the appropriate privileges before beginning.
#

if (( ${UID} != 0 )); then
	fatal "This script must be invoked with 'root' privileges!  Exiting..."
fi


###############################################################################
# Verify that we have access to the various utilities needed by this script.
#

CHATTR=`which chattr`
if (( ${?} != 0 )); then
	fatal "Could not locate 'chattr' utility!  Exiting..."
fi
info "Located '${CHATTR}'"

CHMOD=`which chmod`
if (( ${?} != 0 )); then
	fatal "Could not locate 'chmod' utility!  Exiting..."
fi
info "Located '${CHMOD}'"

CP=`which cp`
if (( ${?} != 0 )); then
	fatal "Could not locate 'cp' utility!  Exiting..."
fi
info "Located '${CP}'"

CUT=`which cut`
if (( ${?} != 0 )); then
	fatal "Could not locate 'cut' utility!  Exiting..."
fi
info "Located '${CUT}'"

DATE=`which date`
if (( ${?} != 0 )); then
	fatal "Could not locate 'date' utility!  Exiting..."
fi
info "Located '${DATE}'"

DF=`which df`
if (( ${?} != 0 )); then
	fatal "Could not locate 'df' utility!  Exiting..."
fi
info "Located '${DF}'"

DU=`which du`
if (( ${?} != 0 )); then
	fatal "Could not locate 'du' utility!  Exiting..."
fi
info "Located '${DU}'"

FIND=`which find`
if (( ${?} != 0 )); then
	fatal "Could not locate 'find' utility!  Exiting..."
fi
info "Located '${FIND}'"

GREP=`which grep`
if (( ${?} != 0 )); then
	fatal "Could not locate 'grep' utility!  Exiting..."
fi
info "Located '${GREP}'"

GZIP=`which gzip`
if (( ${?} != 0 )); then
	fatal "Could not locate 'gzip' utility!  Exiting..."
fi
info "Located '${GZIP}'"

HOSTNAME=`which hostname`
if (( ${?} != 0 )); then
	fatal "Could not locate 'hostname' utility!  Exiting..."
fi
info "Located '${HOSTNAME}'"

LN=`which ln`
if (( ${?} != 0 )); then
	fatal "Could not locate 'ln' utility!  Exiting..."
fi
info "Located '${LN}'"

LSATTR=`which lsattr`
if (( ${?} != 0 )); then
	fatal "Could not locate 'lsattr' utility!  Exiting..."
fi
info "Located '${LSATTR}'"

MKDIR=`which mkdir`
if (( ${?} != 0 )); then
	fatal "Could not locate 'mkdir' utility!  Exiting..."
fi
info "Located '${MKDIR}'"

MV=`which mv`
if (( ${?} != 0 )); then
	fatal "Could not locate 'mv' utility!  Exiting..."
fi
info "Located '${MV}'"

PGREP=`which pgrep`
if (( ${?} != 0 )); then
	fatal "Could not locate 'pgrep' utility!  Exiting..."
fi
info "Located '${PGREP}'"

RM=`which rm`
if (( ${?} != 0 )); then
	fatal "Could not locate 'rm' utility!  Exiting..."
fi
info "Located '${RM}'"

RSYNC=`which rsync`
if (( ${?} != 0 )); then
	fatal "Could not locate 'rsync' utility!  Exiting..."
fi
info "Located '${RSYNC}'"

SED=`which sed`
if (( ${?} != 0 )); then
	fatal "Could not locate 'sed' utility!  Exiting..."
fi
info "Located '${SED}'"

SEQ=`which seq`
if (( ${?} != 0 )); then
	fatal "Could not locate 'seq' utility!  Exiting..."
fi
info "Located '${SEQ}'"

TAIL=`which tail`
if (( ${?} != 0 )); then
	fatal "Could not locate 'tail' utility!  Exiting..."
fi
info "Located '${TAIL}'"

TOUCH=`which touch`
if (( ${?} != 0 )); then
	fatal "Could not locate 'touch' utility!  Exiting..."
fi
info "Located '${TOUCH}'"

XZ=`which xz`
if (( ${?} != 0 )); then
	fatal "Could not locate 'xz' utility!  Exiting..."
fi
info "Located '${XZ}'"




# ------------- the help page ------------------------------------------
if [ "$1" == "-h" ] || [ "$1" == "--help" ]; then
  cat << "EOF"
Version 2.01 2013-01-16

USAGE: rsync-snapshot.sh HOST [--recheck]

PURPOSE: create a snapshot backup of the whole filesystem into the folder
  '/backup/snapshot/HOST/snapshot.001'.
  If HOST is 'localhost' it is replaced with the local hostname.
  If HOST is a remote host then rsync over ssh is used to transfer the files
  with a delta-transfer algorithm to transfer only minimal parts of the files
  and improve speed; rsync uses for this the previous backup as reference.
  This reference is also used to create hard links instead of files when
  possible and thus save disk space. If original and reference file have
  identical content but different timestamps or permissions then no hard link
  is created.
  A rotation of all backups renames snapshot.X into snapshot.X+1 and removes
  backups with X>512. About 10 backups with non-linear distribution are kept
  in rotation; for example with X=1,2,3,4,8,16,32,64,128,256,512.
  The snapshots folders are protected read-only against all users including
  root using 'chattr'.
  The --recheck option forces a sync of all files even if they have same mtime
  & size; it is can verify a backup and fix corrupted files;
  --recheck recalculates also the MD5 integrity signatures without using the
  last signature-file as precalculation.
  Some features like filter rules, MD5, chattr, bwlimit and per server retention
  policy can be configured by modifying the scripts directly.

FILES:
    /backup/snapshot/rsync/rsync-snapshot.sh  the backup script
    /backup/snapshot/rsync/rsync-list.sh      the md5 signature script
    /backup/snapshot/rsync/rsync-include.txt  the filter rules

Examples:
  (nice -5 ./rsync-snapshot.sh >log &) ; tail -f log
  cd /backup/snapshot; for i in $(ls -A); do nice -10 /backup/snapshot/rsync/rsync-snapshot.sh $i; done
EOF
  exit 1
fi



set +x


# ------------- tuning options, file locations and constants -----------
SRC="$1" #name of backup source, may be a remote or local hostname
OPT="$2" #options (--recheck)
SCRIPT_PATH="/backup/snapshot/rsync"
SNAPSHOT_DST="/backup/snapshot" #destination folder
NAME="snapshot" #backup name
LOG="rsync.log"
MIN_MIBSIZE=80000 # older snapshots (except snapshot.001) are removed if free disk <= MIN_MIBSIZE. the script may exit without performing a backup if free disk is still short.
MAX_MIBSIZE=800000 # older snapshots (except snapshot.001) are removed if their size >= MAX_MIBSIZE. the script performs a backup even if their size is too big.

HOST_PORT=22 #port of source of backup
BWLIMIT=100000 # bandwidth limit in KiB/s. 0 does not use slow-down. this allows to avoid rsync consuming too much system performance
BACKUPSERVER="" # this server connects to all other to download filesystems and create remote snapshot backups

# Set to '1' to remove 'snapshot.001' and retry if free disk space is
# insufficient for more than a single snapshot.
OVERWRITE_LAST=0

# Set to '1' to use 'chattr' to protect backups again modification and
# deletion.
DO_CHATTR=1

# Set to '1' to use 'du' command for calculating the size of existing backups.
# Disable it if you have many backups and it is getting too slow.
DO_DU=1

# Set to '1' to compute a list of MD5 integrity signatures of all backuped
# files.  Requires 'rsync-list.sh' script.
DO_MD5=1

# Source directory to snapshot.
SOURCE="/"


# Local and source system hostnames.  If not specified on the command line,
# default to the local hostname.
HOSTNAME_LOCAL="`${HOSTNAME} -s`"
if [ -z "${SRC}" ] || [ "${SRC}" == "localhost" ]; then
	HOSTNAME_SRC="${HOSTNAME_LOCAL}"
else
	HOSTNAME_SRC="${SRC}"
fi


# Fine-tune parameters if running on specific a specific host.
if [ "${HOSTNAME_LOCAL}" == "${BACKUPSERVER}" ]; then
	MIN_MIBSIZE=35000
	MAX_MIBSIZE=12000
	DO_DU=0
	DO_MD5=1
elif [ "${HOSTNAME_LOCAL}" == "${HOSTNAME_SRC}" ]; then
	true
fi


# ------------- initialization -----------------------------------------
shopt -s extglob                                            #enable extended pattern matching operators

RSYNC_OPTIONS="--stats \
	--recursive \
	--links \
	--perms \
	--times \
	--group \
	--owner \
	--devices \
	--hard-links \
	--numeric-ids \
	--delete \
	--delete-excluded \
	--bwlimit=${BWLIMIT}"
#	--progress
#	--size-only
#	--stop-at
#	--time-limit
#	--sparse

# Additional options specific to remote server operation.
if [ "${HOSTNAME_SRC}" != "${HOSTNAME_LOCAL}" ]; then
	SOURCE="${HOSTNAME_SRC}:${SOURCE}"
	RSYNC_OPTIONS="${RSYNC_OPTIONS} \
	--compress \
	--rsh=\"ssh -p ${HOST_PORT} -i /root/.ssh/rsync_rsa -l root\" \
	--rsync-path=\"/usr/bin/rsync\""
fi

if [ "${OPT}" == "--recheck" ]; then
	RSYNC_OPTIONS="${RSYNC_OPTIONS} \
	--ignore-times"
elif [ -n "${OPT}" ]; then
	echo "Try rsync-snapshot.sh --help ."
	exit 2
fi




###############################################################################
# Run-time checks for destination directory and running processes.
#

TIMESTAMP_START=`${DATE} +%s`

# Ensure the snapshot folder already exists.
info "Verifying '${SNAPSHOT_DST}/${HOSTNAME_SRC}' exists..."
if [ ! -d "${SNAPSHOT_DST}/${HOSTNAME_SRC}" ]; then
	fatal "Directory '${SNAPSHOT_DST}/${HOSTNAME_SRC}' is missing.  Exiting..."
fi

# Ensure that there is not already an instance of rsync-snapshot.sh running
# on this host.
info "Verifying 'rsync-snapshot.sh' is not already running..."
if ${PGREP} -af "^/bin/bash.*rsync-snapshot\.sh\$" | ${GREP} -qv "$$"; then
	fatal "Detected an instance of 'rsync-snapshot.sh' already running.  Exiting..."
fi


###############################################################################
# Remove old backups to maintain exponential distribution of prior images.
#
# This loop will iterate through each binary segment (i.e. 512-257, 256-129),
# and keep the snapshot with the _highest_ image number for rotation.
#

#for b in 512 256 128 64 32 16 8 4; do
for b in 16 8; do
	a=$(( b / 2 + 1 ))
	range=`${SEQ} -f"%03g" -s" " ${b} -1 ${a}`
	found=0

	info "Looking for existing snapshots in range ${b}..${a}"
	for i in ${range}; do
		path="${SNAPSHOT_DST}/${HOSTNAME_SRC}/${NAME}.${i}"
		if [ -d "${path}" ]; then
			if [ "${found}" -eq 0 ]; then
				info "Maintaining '${path}' for rotation."
				found=1
			else
				info "Removing '${path}'"
				[ "${DO_CHATTR}" -eq 1 ] && ${CHATTR} -R -i "${path}" &>/dev/null
				${RM} -r "${path}"
			fi
		fi
	done
done

# remove additional backups if free disk space is short
remove_snapshot() {
	MIN_MIBSIZE2=$1
	MAX_MIBSIZE2=$2

	#range=`${SEQ} -f"%03g" -s" " 512 -1 1`
	range=`${SEQ} -f"%03g" -s" " 16 -1 1`

	for i in ${range}; do
		path="${SNAPSHOT_DST}/${HOSTNAME_SRC}/${NAME}.${i}"
		last="${SNAPSHOT_DST}/${HOSTNAME_SRC}/${NAME}.last"

		if [ -d "${path}" ] || [ ${i} -eq 1 ]; then
			echo $path

			[ ! -h "${last}" ] && [ -d "${path}" ] && ${LN} -s "${NAME}.${i}" "${last}"

			info "Checking for available disk space..."
			mib_free=`${DF} -m "${SNAPSHOT_DST}/${HOSTNAME_SRC}/" | ${TAIL} -1 | ${SED} -e 's/  */ /g' | ${CUT} -d" " -f4 | ${SED} -e 's/M*//g'`
			info "Found ${mib_free} MiB free."

			# 0: Space used by snapshots and free disk space are OK
			# 1: Free disk space is too small
			# 2: Disk space used by snapshots is too big
			space=0
			if [ ${mib_free} -ge ${MIN_MIBSIZE2} ]; then
				info "At least ${MIN_MIBSIZE2} MiB availble."

				if [ "${DO_DU}" -eq 0 ]; then
					break
				fi

				info "Checking disk space used by '${SNAPSHOT_DST}/${HOSTNAME_SRC}'..."
				mib_used=`${DU} -ms "${SNAPSHOT_DST}/${HOSTNAME_SRC}/" | ${CUT} -f1`
				info "Found ${mib_used} MiB used."

				if [ ${mib_used} -le ${MAX_MIBSIZE2} ]; then
					info "OK - less than ${MAX_MIBSIZE2} MiB used."
					break
				else
					space=2
				fi
			else
				space=1
			fi

			# Do we need to remove snapshots?
			if [ ${space} -ne 0 ]; then
				if [ ${i} -ne 1 ]; then
					echo "Removing ${path} ..."
					[ "${DO_CHATTR}" -eq 1 ] && ${CHATTR} -R -i "${path}" &>/dev/null
					${RM} -r "${path}"
					[ -h "${last}" ] && ${RM} -f "${last}"
				else #all snapshots except snapshot.001 are removed
					if [ ${space} -eq 1 ]; then #snapshot.001 causes that free space is too small
						if [ "${OVERWRITE_LAST}" -eq 1 ]; then #last chance: remove snapshot.001 and retry once
							OVERWRITE_LAST=0
							echo "Warning, free disk space will be smaller than ${MIN_MIBSIZE} MiB."
							echo "$(date +%Y-%m-%d_%H:%M:%S) OVERWRITE_LAST enabled. Removing ${SNAPSHOT_DST}/${HOSTNAME_SRC}/${NAME}.001 ..."
							${RM} -rf "${SNAPSHOT_DST}/${HOSTNAME_SRC}/${NAME}.001"
							[ -h "${SNAPSHOT_DST}/${HOSTNAME_SRC}/${NAME}.last" ] && ${RM} -f "${SNAPSHOT_DST}/${HOSTNAME_SRC}/${NAME}.last"
						else
							for j in ${LNKDST//--link-dest=/}; do
								if [ -d "${j}" ] && [ "${DO_CHATTR}" -eq 1 ] && [ `${LSATTR} -d "${j}" | ${CUT} -b5` != "i" ]; then
									${CHATTR} -R +i "${j}" &>/dev/null #undo unprotection that was needed to use hardlinks
								fi
							done
							[ ! -h "${SNAPSHOT_DST}/${HOSTNAME_SRC}/${NAME}.last" ] && ${LN} -s "${NAME}.${j}" "${SNAPSHOT_DST}/${HOSTNAME_SRC}/${NAME}.last"
							error "Free disk space would be less than ${MIN_MIBSIZE} MiB."
							error "Exiting..."
							exit 2
						fi
					elif [ ${space} -eq 2 ]; then
						# Even just a single 'snapshot.001' is too large...
						warn "Disk space used by '${SNAPSHOT_DST}/${HOSTNAME_SRC}' will be greater than ${MAX_MIBSIZE} MiB."
						warn "Continuing anyway..."
					fi
				fi
			fi
		fi
	done
}


###############################################################################
# Estimate the amount of required disk space needed for this new backup.
#
# Note that this loop will be executed a 2nd time if OVERWRITE_LAST == 1 and
# snapshot.001 was removed.
#

while :; do
	path="${SNAPSHOT_DST}/${HOSTNAME_SRC}/${NAME}.test-free-disk-space"
	log="${SNAPSHOT_DST}/${HOSTNAME_SRC}/${LOG}"

	OOVERWRITE_LAST="${OVERWRITE_LAST}"

	info "Checking for available free disk space..."

	${MKDIR} -p "${path}"
	${CHMOD} -R 775 "${path}"

	# Unprotect the last snapshot to allow free space check to proceed with
	# hardlinking enabled.
	LNKDST=`${FIND} "${SNAPSHOT_DST}/" -maxdepth 2 -type d -name "${NAME}.001" -printf " --link-dest=%p"`
	for d in ${LNKDST//--link-dest=/}; do
		if [ -d "${d}" ] && [ "${DO_CHATTR}" -eq 1 ] && [ `${LSATTR} -d "${d}" | ${CUT} -b5` == "i" ]; then
			${CHATTR} -R -i "${d}" &>/dev/null
		fi
	done

	eval ${RSYNC} \
		--dry-run \
		${RSYNC_OPTIONS} \
		--include-from="${SCRIPT_PATH}/rsync-include.txt" \
		${LNKDST} \
		"${SOURCE}" \
		"${path}" \
		>"${log}"
	RES=$?
	if [ "${RES}" -ne 0 ] && [ "${RES}" -ne 23 ] && [ "${RES}" -ne 24 ]; then
		fatal "'rsync' execution failed with value ${RES}.  Exiting..."
	fi

	nbytes=$(( `${TAIL} -100 "${log}" | grep 'Total transferred file size:' | ${CUT} -d " " -f5` / (1024 * 1024) ))
	info "Snapshot size estimated as ${nbytes} MiB."

	${RM} -r "${path}" "${log}"

	remove_snapshot $(( ${MIN_MIBSIZE} + ${nbytes} )) $(( ${MAX_MIBSIZE} - ${nbytes} ))
	if [ "${OOVERWRITE_LAST}" == "${OVERWRITE_LAST}" ]; then #no need to retry
		break
	fi
done




# ------------- create the snapshot backup -----------------------------
# perform the filesystem backup using rsync and hard-links to the latest snapshot
# Note:
#   -rsync behaves like cp --remove-destination by default, so the destination
#    is unlinked first.  If it were not so, this would copy over the other
#    snapshot(s) too!
#   -use --link-dest to hard-link when possible with previous snapshot,
#    timestamps, permissions and ownerships are preserved
path="${SNAPSHOT_DST}/${HOSTNAME_SRC}/${NAME}.000"
log="${SNAPSHOT_DST}/${HOSTNAME_SRC}/${LOG}"

info "Creating folder '${path}'"
${MKDIR} -p "${path}"
${CHMOD} 775 "${path}"

info "Backing up '${HOSTNAME_SRC}' to '${path}'"
if [ -n "${LNKDST}" ]; then
	info "Hardlinking with '${LNKDST//\ --link-dest=/}'"
else
	info "Not hardlinking."
fi

eval ${RSYNC} \
	-vv \
	${RSYNC_OPTIONS} \
	--include-from="${SCRIPT_PATH}/rsync-include.txt" \
	${LNKDST} \
	"${SOURCE}" \
	"${path}" \
	>"${log}"
RES=$?
if [ "${RES}" -ne 0 ] && [ "${RES}" -ne 23 ] && [ "${RES}" -ne 24 ]; then
	fatal "'rsync' execution failed with value ${RES}.  Exiting..."
fi

for d in ${LNKDST//--link-dest=/}; do
	if [ -d "${d}" ] && [ "${DO_CHATTR}" -eq 1 ] && [ `${LSATTR} -d "${d}" | ${CUT} -b5` != "i" ]; then
		${CHATTR} -R +i "${d}" &>/dev/null
	fi
done

${XZ} -f "${log}"
${MV} "${log}.xz" "${path}"




# ------------- create the MD5 integrity signature ---------------------
# create a gziped 'find'-list of all snapshot files (including md5 signatures)
if [ "${DO_MD5}" -eq 1 ]; then
  info "Computing filelist with MD5 signatures for '${SNAPSHOT_DST}/${HOSTNAME_SRC}/${NAME}.000'..."
  OWD="$(pwd)"
  cd "${SNAPSHOT_DST}"
#  NOW=$(date "+%s")
#  MYTZ=$(date "+%z")
#  let NOW${MYTZ:0:1}=3600*${MYTZ:1:2}+60*${MYTZ:3:2} # convert localtime to UTC
#  DATESTR=$(date -d "1970-01-01 $((${NOW} - 1)) sec" "+%Y-%m-%d_%H:%M:%S") # 'now - 1s' to avoid missing files
  DATESTR=$(date -d "1970-01-01 UTC $(($(date +%s) - 1)) seconds" "+%Y-%m-%d_%H:%M:%S") # 'now - 1s' to avoid missing files
  REF_LIST="$(${FIND} ${SNAPSHOT_DST}/${HOSTNAME_SRC}/${NAME}.001/ -maxdepth 1 -type f -name 'snapshot.*.list.xz' 2>/dev/null)"
  if [ -n "${REF_LIST}" ] && [ "${OPT}" != "--recheck" ]; then
    REF_LIST2="/tmp/rsync-reflist.tmp"
    ${XZ} -dc "${REF_LIST}" >"${REF_LIST2}"
    ${TOUCH} -r "${REF_LIST}" "${REF_LIST2}"
    ${SCRIPT_PATH}/rsync-list.sh "${HOSTNAME_SRC}/${NAME}.000" 0 "${REF_LIST2}" | sort -u | ${XZ} -c >"${HOSTNAME_SRC}/${NAME}.${DATESTR}.list.xz"
    ${RM} -f "${REF_LIST2}"
  else
    ${SCRIPT_PATH}/rsync-list.sh "${HOSTNAME_SRC}/${NAME}.000" 0 | sort -u | ${XZ} -c >"${HOSTNAME_SRC}/${NAME}.${DATESTR}.list.xz"
  fi
  ${TOUCH} -d "${DATESTR/_/ }" "${SNAPSHOT_DST}/${HOSTNAME_SRC}/${NAME}.${DATESTR}.list.xz"
  cd "${OWD}"

	[ ! -d "${SNAPSHOT_DST}/${HOSTNAME_SRC}/md5-log" ] && ${MKDIR} -p "${SNAPSHOT_DST}/${HOSTNAME_SRC}/md5-log"
	${CP} -al "${SNAPSHOT_DST}/${HOSTNAME_SRC}/${NAME}.${DATESTR}.list.xz" "${SNAPSHOT_DST}/${HOSTNAME_SRC}/md5-log/${NAME}.${DATESTR}.list.xz"
	${MV} "${SNAPSHOT_DST}/${HOSTNAME_SRC}/${NAME}.${DATESTR}.list.xz" "${SNAPSHOT_DST}/${HOSTNAME_SRC}/${NAME}.000/${NAME}.${DATESTR}.list.xz"
	${TOUCH} -d "${DATESTR/_/ }" "${SNAPSHOT_DST}/${HOSTNAME_SRC}/${NAME}.000"
fi




###############################################################################
# Finish and clean-up.
#
# Protect the backup against modification with 'chattr +immutable'.
#

path="${SNAPSHOT_DST}/${HOSTNAME_SRC}/${NAME}.000"
if [ "${DO_CHATTR}" -eq 1 ]; then
	info "Setting recursively immutable flag on '${path}'"
	${CHATTR} -R +i "${path}" &>/dev/null
fi

#path="${SNAPSHOT_DST}/${HOSTNAME_SRC}/${NAME}.512"
path="${SNAPSHOT_DST}/${HOSTNAME_SRC}/${NAME}.16"
if [ -d "${path}" ]; then
	info "Removing '${path}'..."
	[ "${DO_CHATTR}" -eq 1 ] && ${CHATTR} -R -i "${path}" &>/dev/null
	${RM} -r "${path}"
fi

last="${SNAPSHOT_DST}/${HOSTNAME_SRC}/${NAME}.last"
[ -h "${last}" ] && ${RM} -f "${last}"

#range=`${SEQ} -f"%03g" -s" " 511 -1 0`
range=`${SEQ} -f"%03g" -s" " 15 -1 0`
echo ${range}
for i in ${range}; do
	if [ -d "${SNAPSHOT_DST}/${HOSTNAME_SRC}/${NAME}.${i}" ]; then
		let j=${i##+(0)}+1
		j=$(printf "%.3d" "${j}")
		info "Rotating ${SNAPSHOT_DST}/${HOSTNAME_SRC}/${NAME}.${i} --> ${SNAPSHOT_DST}/${HOSTNAME_SRC}/${NAME}.${j}"
		[ "${DO_CHATTR}" -eq 1 ] && ${CHATTR} -i "${SNAPSHOT_DST}/${HOSTNAME_SRC}/${NAME}.${i}" &>/dev/null
		${MV} "${SNAPSHOT_DST}/${HOSTNAME_SRC}/${NAME}.${i}" "${SNAPSHOT_DST}/${HOSTNAME_SRC}/${NAME}.${j}"
		[ "${DO_CHATTR}" -eq 1 ] && ${CHATTR} +i "${SNAPSHOT_DST}/${HOSTNAME_SRC}/${NAME}.${j}" &>/dev/null
		[ ! -h "${SNAPSHOT_DST}/${HOSTNAME_SRC}/${NAME}.last" ] && ${LN} -s "${NAME}.${j}" "${SNAPSHOT_DST}/${HOSTNAME_SRC}/${NAME}.last"
	fi
done

# remove additional backups if free disk space is short
OVERWRITE_LAST=0 #next call of remove_snapshot() will not remove snapshot.001
remove_snapshot ${MIN_MIBSIZE} ${MAX_MIBSIZE}



###############################################################################
# Done.
#

TIMESTAMP_STOP=`${DATE} +%s`
info "Snapshot successfully created in $(( ${TIMESTAMP_STOP} - ${TIMESTAMP_START} )) seconds."
info ""
info "Snapshot may be de-dup'd with:"
info "    sudo chattr -R -i \"${SNAPSHOT_DST}/${HOSTNAME_SRC}/${NAME}.001\""
info "    sudo /usr/share/fslint/fslint/findup -m \"${SNAPSHOT_DST}/${HOSTNAME_SRC}/${NAME}.001\""
info "    sudo chattr -R +i \"${SNAPSHOT_DST}/${HOSTNAME_SRC}/${NAME}.001\""
info ""
info "This is only really useful on an initial (or changed) backup set."
info "Afterwards, an comparison of total vs. de-dup'd usgage may be made with:"
info "    sudo du -chslB1M ${SNAPSHOT_DST}/${HOSTNAME_SRC}/${NAME}.*"
info "    sudo du -chsB1M ${SNAPSHOT_DST}/${HOSTNAME_SRC}/${NAME}.*"
info ""
info "Done."
exit 0

